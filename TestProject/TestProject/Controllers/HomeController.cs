﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestProject.Models;

namespace TestProject.Controllers
{
    public class HomeController : Controller
    {
        BiographyDBEntities2 _dbContext = new BiographyDBEntities2();


        public ActionResult Index()
        {
            var categoryInfo = _dbContext.Categories.FirstOrDefault(f => f.Category_ID == 1);
            return View(categoryInfo);
        }
    }

}