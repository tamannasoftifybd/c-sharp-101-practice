﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoSums
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int n,target,flag = 0;
            int[] inputs = new int[200];
            int[] outputs = new int[2];
            Console.WriteLine("Enter a number:");
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter inputs: \n");
            for(int i = 0; i < n; i++)
            {
                inputs[i] = Convert.ToInt32(Console.ReadLine());
            
            }
            Console.WriteLine("Enter target:\n");
            target = Convert.ToInt32(Console.ReadLine());
            for(int i =0;i< inputs.Length; i++)
            {
                for(int j = i+1; j < inputs.Length; j++)
                {
                    if (inputs[i] + inputs[j] == target)
                    {
                        outputs[0] = i;
                        outputs[1] = j;
                        flag++;
                        break;
                    }
                }
                if (flag != 0)
                    break;
            }
            Console.WriteLine("Outputs");
            for(int i = 0; i < outputs.Length; i++)
            {
                Console.WriteLine(outputs[i]);
            }
            Console.ReadLine();
        }
    }
}
