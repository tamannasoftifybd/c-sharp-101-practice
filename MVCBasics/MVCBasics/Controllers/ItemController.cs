﻿using MVCBasics.Gateway;
using MVCBasics.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace MVCBasics.Controllers
{
    
    public class ItemController : Controller
    {
        ItemGateway aGateway = new ItemGateway();
        public string save(Item item)
        {
           int rowEffected = aGateway.save(item);

            if(rowEffected > 0)
            {
                return "Item Saved";
            }
            return "Saving Failed";
        }
        // GET: Item
       //public string find(int? id)
       // {
       //     if(id == null)
       //     {
       //         return "Item not found";
       //     }
       //     else
       //     {
       //         return "Item found";
       //     }
       // }
        //public string find(Item item)
        //{
        //    if(item == null)
        //    {
        //        return "Item not found";
        //    }
        //    else
        //    {
        //        return item.Id + " " + item.Name + " " + item.Price;
        //    }
        //}
        public List<Item> GetALLItems()
        {
            List<Item> items = new List<Item>()
            {
                new Item(){Id=1, Name="Chocolate",Price=120},
                new Item(){Id=2, Name="Pizza",Price=600},
                new Item(){Id=3, Name="Coffee",Price=80}
            };

            return items;
        }
        public string find(int? id)
        {
            List<Item> items = GetALLItems();

            Item aItem = items.Find(item => item.Id == id);

            if(aItem == null)
            {
                return "No item found with this ID";
            }
            else
            {
                return "Item : " + aItem.Name + "Price : " + aItem.Price;
            }

        }
    }
}