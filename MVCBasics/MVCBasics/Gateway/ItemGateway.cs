﻿using MVCBasics.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace MVCBasics.Gateway
{
    public class ItemGateway
    {
        public int save(Item item)
        {
            string connectionString =
                WebConfigurationManager.ConnectionStrings["ItemDBConnectionString"].ConnectionString;

            SqlConnection connection = new SqlConnection(connectionString);

            string query = "INSERT INTO Item VALUES(@Name,@Price)";

            SqlCommand command = new SqlCommand(query, connection);

            command.Parameters.Clear();
            command.Parameters.Add("Name", System.Data.SqlDbType.VarChar);
            command.Parameters["Name"].Value = item.Name;

            command.Parameters.Add("Price", System.Data.SqlDbType.Decimal);
            command.Parameters["Price"].Value = item.Price;

            connection.Open();

            int rowEffected = command.ExecuteNonQuery();

            return rowEffected;
        }
    }
}