﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicPractice
{
    internal class Program
    {
        static string myString = "This is first code";
        static void Main(string[] args)
        {

            DataTypesandTypeConversionIntro();
            OperatorsIntro();
            ReplaceAndFindLocatin();
            TwoSums();

            Console.ReadKey();
        }
        //Data Types Practice by Tamanna
        static void DataTypesandTypeConversionIntro()
        {
            Console.WriteLine("-------Primitive Data Types--------");
            int myInt = 35;
            double myDouble = 2233.7665;
            char myChar = 'a';
            
            Console.WriteLine("Integer : " + myInt + "\nDouble : " + myDouble + "\nCharacter : " + myChar + "\nString : " + myString);

            Console.WriteLine("--------Type Casting---------");
            int a = myChar;
            double d = myInt;
            Console.WriteLine("Implicit Casting : " + a + ", " + d);

            int b = (int)myDouble;
            //int c = Convert.ToInt32(myString);
            //double e = Convert.ToDouble(myString);
            string s = myDouble.ToString();
            string g = myInt.ToString();
            Console.WriteLine("Explicit Casting : " + b + ", " + s + ", " + ", " + g);

        }

        //Oparators Practice by Tamanna
        static void OperatorsIntro()
        {
            Console.WriteLine("------Arithmatic Operators--------");
            int firstNumber = Convert.ToInt32(Console.ReadLine());
            int secondNumber = Convert.ToInt32(Console.ReadLine());

            int sum = firstNumber + secondNumber;
            int subtract = firstNumber - secondNumber;
            int multiplication = firstNumber * secondNumber;
            int division = firstNumber / secondNumber;
            int modulus = firstNumber % secondNumber;

            Console.WriteLine("Summation : " + sum + "\nSubtraction : " + subtract + "\nMultiplication : " + multiplication +
                "\nDivision : " + division + "\nModulus : " + modulus);

            Console.WriteLine("-------Comparison Operators--------");
            if (firstNumber == secondNumber)
                Console.WriteLine("Both numbers are equal");
            else if (firstNumber > secondNumber)
                Console.WriteLine("First Number is greater than Second Number");
            else if (firstNumber < secondNumber)
                Console.WriteLine("First Number is less than Second Number");

            Console.WriteLine("-----Logical Operator------");
            if (firstNumber == secondNumber && firstNumber % secondNumber == 0)
                Console.WriteLine("True");
            else
                Console.WriteLine("False");
        }

        //Replace And Find Location In a string by Tamanna
        static void ReplaceAndFindLocatin()
        {
            Console.WriteLine("-----Find Loaction and replace in a string--------");
            int[] loc = new int[20] ;
            char[] myChars = myString.ToCharArray();
            for (int i = 0; i < myChars.Length; i++)
            {
                if (myChars[i] == 'i')
                {
                    myChars[i] = 'l';
                    loc[i] = i;
                }
               
            }
            Console.WriteLine("New String : ");
            Console.WriteLine(myChars);
            Console.WriteLine("Location: ");
            for(int i = 0;i< loc.Length; i++)
            {
                if (loc[i] != 0)
                 Console.WriteLine(loc[i]);
            }
        }

        //Find location of two value which generate a taget value by summation by Tamanna
        static void TwoSums()
        {
            int n, target, flag = 0;
            int[] inputs = new int[200];
            int[] outputs = new int[2];
            Console.WriteLine("Enter a number:");
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter inputs: \n");
            for (int i = 0; i < n; i++)
            {
                inputs[i] = Convert.ToInt32(Console.ReadLine());

            }
            Console.WriteLine("Enter target:\n");
            target = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < inputs.Length; i++)
            {
                for (int j = i + 1; j < inputs.Length; j++)
                {
                    if (inputs[i] + inputs[j] == target)
                    {
                        outputs[0] = i;
                        outputs[1] = j;
                        flag++;
                        break;
                    }
                }
                if (flag != 0)
                    break;
            }
            Console.WriteLine("Outputs");
            for (int i = 0; i < outputs.Length; i++)
            {
                Console.WriteLine(outputs[i]);
            }
            Console.ReadLine();
        }
    }
}
