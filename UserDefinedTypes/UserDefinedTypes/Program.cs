﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDefinedTypes
{
    internal class Program
    {
        static void Main(string[] args)
        {
            BasicUserDefinedTypes();
            UserDefinedTypeWithArray();
            GetFullName();
            UsingConstructor();
            InheritanceIntro();
            Polymorphism();
            
            Console.ReadKey();
        }

        //User Defined Data Type by Tamanna
        static void BasicUserDefinedTypes()
        {
            Console.WriteLine("-----Enter persons details using user input------");
            Person aPerson = new Person();
            aPerson.Name = Console.ReadLine();
            aPerson.Age = Convert.ToInt32(Console.ReadLine());
            aPerson.Address = Console.ReadLine();
            aPerson.ContactNo = Console.ReadLine();

            Console.WriteLine(aPerson.Name);
            Console.WriteLine(aPerson.Age);
            Console.WriteLine(aPerson.Address);
            Console.WriteLine(aPerson.ContactNo);
        }

        //User defined types with array by Tamanna
        static void UserDefinedTypeWithArray()
        {
            Console.WriteLine("-----Enter multiple perosons data-----");
            Person[] persons = new Person[2];
            for (int i = 0; i < persons.Length; i++)
            {
                Person person = new Person();
                person.Name = Console.ReadLine();
                person.Age = Convert.ToInt32(Console.ReadLine());
                person.Address = Console.ReadLine();
                person.ContactNo = Console.ReadLine();

                persons[i] = person;
            }

            foreach (Person person in persons)
            {
                Console.WriteLine(person.Name);
                Console.WriteLine(person.Age);
                Console.WriteLine(person.Address);
                Console.WriteLine(person.ContactNo);
            }
        }
        
        //Access a method in a class by Tamanna
        static void GetFullName()
        {
            Console.WriteLine("----Get a full name-------");
            Person aPerson = new Person();
            aPerson.FirstName = Console.ReadLine();
            aPerson.LastName = Console.ReadLine();
            Console.WriteLine(aPerson.FullName(aPerson.FirstName, aPerson.LastName));
        }

        //Constructor by Tamanna
        static void UsingConstructor()
        {
            Console.WriteLine("-----Car Details using constructor----");
            string carName = Console.ReadLine();
            string model = Console.ReadLine();
            string color = Console.ReadLine();
            Car aCar = new Car(carName,model,color);
            Console.WriteLine("Car Name  : " + carName + "\nModel : " + model + "\nColor : "  + color);
        }

        //Inheritance By Tamanna
        static void InheritanceIntro()
        {
            Console.WriteLine("-----Student Details with result-----");
            double[] credit = new double[10];
            double[] gpa = new double[10];
            StudentResult result = new StudentResult();
            result.Name = Console.ReadLine();
            result.StudentId = Console.ReadLine();
            result.Email = Console.ReadLine();
            for (int i=0; i < 5; i++)
            {
                credit[i] = Convert.ToDouble(Console.ReadLine());
                gpa[i] = Convert.ToDouble(Console.ReadLine());
            }
            Console.WriteLine(result.GetGPA(credit, gpa));
        }

        //Polymorphism by Tamanna
        static void Polymorphism()
        {
            Console.WriteLine("----Payment History using polymorphism----");
            Student aStudent = new Student();
            Student payment = new StudentPayment();
            
            Console.WriteLine(payment.CheckPayment(20000,20000));
        }
    }
}
