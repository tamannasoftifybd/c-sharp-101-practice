﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDefinedTypes
{
    internal class Student
    {
        public string Name { get; set; }   
        public string StudentId { get; set; }
        public string Email { get; set; }
        public virtual string CheckPayment(double requiredPayment, double paidAmonut)
        {
            return "Your payment is cleared!";
        }
    }
   
}
