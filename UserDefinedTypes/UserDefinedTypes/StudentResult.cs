﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDefinedTypes
{
    internal class StudentResult : Student
    {

        public double GetGPA(double[] credit,double[] gpa)
        {
            double cgpa=0.0, totalCredit=0.0;
             for(int i=0; i <5; i++)
             {
                cgpa += credit[i] * gpa[i];
                totalCredit += credit[i];
             }
            double totalCGPA = cgpa / totalCredit;
            return totalCGPA;
        }
    }
}
