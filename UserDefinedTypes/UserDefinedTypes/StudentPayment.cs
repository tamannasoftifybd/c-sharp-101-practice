﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDefinedTypes
{
    internal class StudentPayment : Student
    {
       
        public override string CheckPayment(double requiredPayment, double paidAmount)
        {
            if (requiredPayment - paidAmount > 0)
                return "Your Payment is not clear";
            else
                return base.CheckPayment(requiredPayment, paidAmount);
        }
    }
}
