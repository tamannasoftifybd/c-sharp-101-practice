﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDefinedTypes
{
    internal class Car
    {
        public string CarName { get; set; }
        public string Model { get; set; }
        public string Color { get; set;}

        public Car()
        {

        }
        public Car(string carName, string model, string color)
        {
            CarName = carName;
            Model = model;
            Color = color;
        }   
    }
}
