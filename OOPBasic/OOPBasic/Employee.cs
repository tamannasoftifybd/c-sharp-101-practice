﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPBasic
{
    internal class Employee
    {
        static string company = "ABC";
        public int empID;
        public string empName;
        public string email;
        private int a;
        private int b;
        private int c;
        static Employee()
        {
            Console.WriteLine("Company Name is: " + company);
        }
        public Employee(int a, int b, int c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            Console.WriteLine("this is first constructor " + (this.a + this.b + this.c));
        }
        public Employee(int a, int b)
        {
            this.a = a;
            this.b = b;
            Console.WriteLine("this is second constructor " + (this.a+this.b));
        }

        public Employee()
        {
            Console.WriteLine("this is default constructor");
        }
    }
}
