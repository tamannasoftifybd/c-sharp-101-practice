﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPBasic
{
    internal class Program
    {
        //int id;
        //string name;
        //string description;
        //string isbn;

        //public void SetBook(int id, string name,string description,string isbn)
        //{
        //    this.id = id;
        //    this.name = name;
        //    this.description = description;
        //    this.isbn = isbn;
        //}
        
        //public void GetBook()
        //{
        //    Console.WriteLine(id);
        //    Console.WriteLine(name);
        //    Console.WriteLine(description);
        //    Console.WriteLine(isbn);
        //}
        static void Main(string[] args)
        {
            //Book aBook = new Book();
            //aBook.SetBook(1, "Life and Death", "Fantasy", "978-3-16");
            //aBook.GetBook();

            ConstructorTypesAndOverloading();
            ConstructorDependencyInjection();   
            Console.ReadKey();
        }

        //Constrstor types and overloading by Tamanna
        static void ConstructorTypesAndOverloading()
        {
            Console.WriteLine("------Constructor------");
            Employee aEmployee = new Employee(2,5,6);
            Employee e = new Employee(4,5);
            Employee employee = new Employee();
            Employee emp = new Employee();
        }

        static void ConstructorDependencyInjection()
        {
            Console.WriteLine("--------Constructor Dependency--------");
            Service1 s1 = new Service1();
            Client c1 = new Client(s1);
            Console.WriteLine(c1.ServeMethod());
            Console.ReadKey();

        }
    }
}
