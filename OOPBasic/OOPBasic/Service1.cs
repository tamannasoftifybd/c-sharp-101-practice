﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPBasic
{
    internal class Service1 : IService
    {
        public string Serve()
        {
            return "Service1 called!";
        }
    }
}
