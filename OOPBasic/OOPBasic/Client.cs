﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPBasic
{
    internal class Client
    {
        private IService _service;
        public Client(IService service)
        {
            _service = service;
        }
        public string ServeMethod()
        {
           return _service.Serve();
        }
    }
}
