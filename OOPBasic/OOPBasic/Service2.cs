﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPBasic
{
    internal class Service2 : IService
    {
        public string Serve()
        {
            return "Service2 called!";
        }
    }
}
